import {Framework} from "floose";
import ComponentConfiguration = Framework.ComponentConfiguration;
import Model = Framework.Data.Model;
import {User} from "floose-module-users";

export declare enum DriverType {
    MSSQL = "mssql",
    MYSQL = "mysql"
}
export declare enum MimeType {
    IMAGE_GIF = "image/gif",
    IMAGE_JPG = "image/jpeg",
    IMAGE_PNG = "image/png"
}

/**
 * CONFIGURATION
 */
export interface ChatModuleConfig extends ComponentConfiguration {
    driver: DriverType;
    connection: string
}

/**
 * MANAGERS
 */
export declare class MessageManager {
    protected constructor();
    static getInstance(): MessageManager;

    getMessageById(id: string | string[]): Promise<Message | Message[]>;
    getMessageByUserAndRoom(user: User, room: Room, dateFrom?: Date, dateTo?: Date): Promise<{
        message: Message;
        deliveredAt?: Date;
        readAt?: Date;
    }[]>;
    createMessage(sender: User, room: Room, content: string, attachments?: Attachment[]): Promise<Message>;
    setMessageReadForUser(message: Message, user: User): Promise<void>;
    deleteMessage(message: Message | Message[]): Promise<void>;
    getAttachmentById(id: string | string[]): Promise<Attachment | Attachment[]>;
    createAttachment(file: Buffer, mimetype: MimeType): Promise<Attachment>;
    deleteAttachment(attachment: Attachment | Attachment[]): Promise<void>;
}
export declare class RoomManager {
    protected constructor();
    static getInstance(): RoomManager;

    getRoomById(id: string | string[]): Promise<Room | Room[]>;
    getRoomByUser(user: User): Promise<Room[]>;
    createRoom(name: string, users: User[]): Promise<Room>;
    addUserToRoom(user: User, room: Room): Promise<Room>;
    removeUserFromRoom(user: User, room: Room): Promise<Room>;
    deleteRoom(room: Room | Room[]): Promise<void>;
}

/**
 * MODELS
 */
export declare class AttachmentSchema {
    static readonly TABLE_NAME: string;
}
export declare class Attachment extends Model {
    id: string;
    path: string;
    createdAt: Date;
    updatedAt: Date;
}
export declare class MessageSchema {
    static readonly TABLE_NAME: string;
}
export declare class Message extends Model {
    id: string;
    room: string;
    sender: string;
    content: string;
    attachments: string[];
    createdAt: Date;
    updatedAt: Date;
}
export declare class RecipientSchema {
    static readonly TABLE_NAME: string;
}
export declare class Recipient extends Model {
    id: string;
    user: string;
    message: string;
    deliveredAt: Date;
    readAt: Date;
    createdAt: Date;
    updatedAt: Date;
}
export declare class RoomSchema {
    static readonly TABLE_NAME: string;
}
export declare class Room extends Model {
    id: string;
    name: string;
    users: string[];
    createdAt: Date;
    updatedAt: Date;
}